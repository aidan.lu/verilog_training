module half_adder(
	input wire A,
	input wire B,
	output wire S,
	output wire C_out
);

	and (C_out, A, B);	// Carry output
	xor (S, A, B);	// Sum output

endmodule

module full_adder_3(
	input wire A,
	input wire B,
	input wire C_in,
	
	output wire S,
	output wire C_out
);


	wire S1;	//1'st half adder Sum output
	wire C1;	//1'st half adder Carry output

	wire C2;	//2'nd half adder Carry output

	//1'st half adder
	half_adder first_half_adder(A, B, S1, C1);


	//2'nd half adder
	half_adder second_half_adder(S1, C1, S, C2);


	or (C_out, C1, C2);// Carry output

endmodule
